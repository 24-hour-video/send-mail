const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect
const should = chai.should()
const assert = chai.assert

const testEvent = {
  to: "balint.sera@gmail.com",
  subject: "SES test",
  body: "Hi brother, this is a test",
}

describe('SES Mail sends mail', () => {
  describe('#execute', () => {
    let eventStub, module;
    before(async () => {
      eventStub = sinon.stub().yields(null, testEvent)
      module = getModule()

    })

    it('should send mail', async () => {
      const response = await module.handler(testEvent, null);
      expect(response.status).to.equal('OK')
    })

    it('should not send mail if the event is not well-formed', async () => {
      delete testEvent.subject
      const response = await module.handler(testEvent, null);
      expect(response.error.message).to.equal('Invalid input {"to":"balint.sera@gmail.com","body":"Hi brother, this is a test"}')
    })
  })
});

const getModule = () => {
  const rewired = rewire('../index.js')
  const orig = require('../service/SESMail')
  orig.prototype.send = () => { return Promise.resolve() }
  rewired.__set__({
    'SESMail': orig
  })

  return rewired
}