class Message {
  constructor(to, subject, body, from = false) {
    this.to = Array.isArray(to) ? to : [to]
    this.subject = subject
    this.body = body
    this.from = from || 'balint.sera@gmail.com'
  }
}
module.exports = Message;