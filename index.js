'use strict';
const SESMail = require('./service/SESMail')
const Joi = require('joi')

exports.handler = async (event, context) => {
  try {
    const valid = validate(event)
    if (valid.error) {
      throw new Error('Invalid input ' + JSON.stringify(event))
    }

    const sender = SESMail.factory(event.to, event.subject, event.body || "")

    await sender.send()
    return { status: "OK" }
  } catch(err) {
    return { error: err }
  }
}


const validate = (event) => {
  const schema = Joi.object().keys({
    to: Joi.string().email().required(),
    subject: Joi.string().max(120).required(),
    body: Joi.string().max(1000)
  })

  return Joi.validate(event, schema)
}

