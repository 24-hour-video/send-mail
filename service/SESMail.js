const AWS = require('aws-sdk')
AWS.config.update({ region: process.env.REGION })

const SES = new AWS.SES()
const Message = require('../model/Message')

/**
 * SES mail sender
 */
class SESMail {
  /**
   *
   * @returns {SESMail}
   */
  static factory(to, subject, body) {
    const message = new Message(to, subject, body)
    return new SESMail(message)
  }
  /**
   *
   * @param message model/Message
   * @param sender
   */
  constructor (message) {
    this.message = message
  }

  /**
   * send mail
   * @return {Promise<PromiseResult<PromiseResult<D, E>, E>>}
   */
  send () {
    return SES.sendEmail(this.params).promise()
  }

  /**
   * Return message as SES formatted params
   * @returns {{Source: *, Destination: {ToAddresses: Health.timestamp | CloudWatchLogs.Timestamp | *}, Message: {Subject: {Data: Lightsail.NonEmptyString | Support.Subject | SNS.subject}, Body: {Text: {Data: *}}}}}
   */
  get params () {
    return {
      Source: this.message.from,
      Destination: { ToAddresses: this.message.to },
      Message: {
        Subject: {
          Data: this.message.subject
        },
        Body: {
          Text: {
            Data: this.message.body
          }
        }
      }
    }
  }

}

module.exports = SESMail